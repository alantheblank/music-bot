import discord
from discord.ext import commands
import asyncio
import spotipy
import json
from spotipy.oauth2 import SpotifyClientCredentials
import wavelink
import time


class Logger(object):
    def debug(self, msg):
        print(msg)

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)

# Or that the pull/merge request couldn't be on our own repos

class Commands(commands.Cog):

    vc: wavelink.Player = None

    def __init__(self, bot):
        self.bot: commands.Bot = bot
        self.bot.loop.create_task(self.connect())
        with open("api.json", "r") as f:
            api: dict = json.load(f)
            auth_manager = SpotifyClientCredentials(client_id=api["spotify_client_id"], client_secret=api["spotify_client_secret"])
        self.spot = spotipy.Spotify(auth_manager=auth_manager)

    @commands.slash_command(name="ping", description="Replies with pong!")
    async def ping(self, interaction: discord.Interaction):
        await interaction.response.send_message("Pong!")

    @discord.slash_command(name="play", description="Plays that funky beat")
    async def play(self, interaction: discord.Interaction, search: str):
        await interaction.defer()
        if not interaction.user.voice:
            await interaction.send_followup("You are not in a voice channel")
        elif not interaction.guild.voice_client:
            self.vc = await interaction.user.voice.channel.connect(cls=wavelink.Player)

        try:
            match search:
                case search if "youtube.com" in search:
                    if "&list=" in search:
                        tracklist = await self.vc.node.get_playlist(cls=wavelink.YouTubePlaylist, identifier=search)
                        track = tracklist.tracks
                    else:
                        track = await self.vc.node.get_tracks(cls=wavelink.YouTubeMusicTrack, query=search)
                case search if "spotify" in search:
                    if "track" in search:
                        result = self.spot.track(search)
                        track = []
                        trackname = f"{result['artists'][0]['name']} - {result['name']}"
                        print(trackname)
                        tracks = await self.vc.node.get_tracks(cls=wavelink.YouTubeMusicTrack,
                                                                   query=f"ytsearch: {trackname}")
                        track.append(tracks[0])
                    if "playlist" in search:
                        results = self.spot.playlist(search)
                        track = []
                        for result in results["tracks"]["items"]:
                            trackname = f"{result['track']['artists'][0]['name']} - {result['track']['name']}"
                            #print(trackname)
                            tracks = await self.vc.node.get_tracks(cls=wavelink.YouTubeMusicTrack, query=f"ytsearch: {trackname}")
                            track.append(tracks[0])
                case _:
                    tracks = await self.vc.node.get_tracks(cls=wavelink.YouTubeMusicTrack, query=f"ytsearch: {search}")
                    print(tracks)
                    track = [tracks[0]]
                    print(track)

            if len(track) > 1:
                for t in track:
                    if interaction.guild.voice_client.is_playing():
                        self.vc.queue.put(item=t)
                    else:
                        await self.vc.play(t)
                await interaction.send_followup(f"Queued {len(track)} songs")
            else:
                if interaction.guild.voice_client.is_playing():
                    self.vc.queue.put(item=track[0])
                    await interaction.send_followup(f"Queued {track[0].title}")
                else:
                    self.vc.queue.put(item=track[0])
                    await self.vc.play(self.vc.queue.get())
                    await interaction.send_followup(f"Queued {self.vc.source.title}")
        except Exception as e:
            print(e)
            await interaction.send_followup("Something went wrong! Please try again!")

    @discord.slash_command(name="bellaciao", description="Sure, why not")
    async def ciao(self, interaction: discord.Interaction):
        if not interaction.user.voice:
            await interaction.send_followup("You are not in a voice channel")
        elif not interaction.guild.voice_client:
            self.vc = await interaction.user.voice.channel.connect(cls=wavelink.Player)

        song = await self.vc.node.get_tracks(cls=wavelink.YouTubeMusicTrack, query=f"ytsearch: https://www.youtube.com/watch?v=CwCIvSdGQZE")
        if interaction.guild.voice_client.is_playing():
            self.vc.queue.put(item=song[0])
            await interaction.response.send_message("Queued Bella Ciao")
        else:
            self.vc.queue.put(item=song[0])
            await self.vc.play(self.vc.queue.get())
            await interaction.response.send_message("Playing Bella Ciao")

    @discord.slash_command(name="join", description="Joins the current voice channel of the invoker")
    async def join(self, interaction: discord.Interaction):
        print(interaction)
        user = interaction.user
        if user.voice:
            print(user.voice)
            channel = user.voice.channel
            if not interaction.guild.voice_client:
                self.vc: wavelink.Player = await channel.connect(cls=wavelink.Player)
                await interaction.guild.change_voice_state(channel=channel, self_deaf=True)
                await interaction.response.send_message(f"Joined {channel.name}")
            else:
                await interaction.response.send_message(f"Already in a voice channel")
        else:
            await interaction.response.send_message("You are not in a voice channel!")

    @discord.slash_command(name="leave", description="Makes the bot leave their current voice channel")
    async def leave(self, interaction: discord.Interaction):
        if self.vc:
            await self.vc.disconnect(force=True)
        else:
            await interaction.response.send_message("Bot is not in a voice channel")

    @discord.slash_command(name="stop", description="Stops playback")
    async def stop(self, interaction: discord.Interaction):
        if self.vc:
            await self.vc.disconnect(force=True)
            await interaction.response.send_message("Leaving Voice Chat")
        else:
            await interaction.response.send_message("Bot is not in a voice channel")

    @discord.slash_command(name="skip", description="Skips current song")
    async def skip(self, interaction: discord.Interaction):
        if self.vc:
            if not self.vc.is_playing():
                return await interaction.response.send_message("Bot is not playing music")
            if self.vc.queue.is_empty:
                await interaction.response.send_message(f"Skipped {self.vc.track.title}")
                return await self.vc.stop()

            await interaction.response.send_message(f"Skipped {self.vc.track.title}")
            await self.vc.seek(self.vc.track.length * 1000)
            if self.vc.is_paused():
                await self.vc.resume()
        else:
            await interaction.response.send_message("Bot is not in a voice channel")

    @discord.slash_command(name="pause", description="Pauses the current song")
    async def pause(self, interaction: discord.Interaction):
        if self.vc:
            if self.vc.is_paused():
                await self.vc.resume()
                await interaction.response.send_message("resumed playback")
            else:
                await self.vc.pause()
                await interaction.response.send_message("paused playback")
        else:
            await interaction.response.send_message("Bot is not in a voice call")

    @discord.slash_command(name="resume", description="Resumes the current song")
    async def resume(self, interaction: discord.Interaction):
        if self.vc:
            if self.vc.is_paused():
                await self.vc.resume()
                await interaction.response.send_message("resumed playback")
            else:
                await self.vc.pause()
                await interaction.response.send_message("paused playback")
        else:
            await interaction.response.send_message("Bot is not in a voice call")

    @discord.slash_command(name="np", description="Shows what's currently playing")
    async def np(self, interaction: discord.Interaction):
        embed = discord.Embed()
        if self.bot.user.avatar:
            embed.set_author(name=self.bot.user.name, icon_url=self.bot.user.avatar)
        else:
            embed.set_author(name=self.bot.user.name)
        embed.add_field(name="Now Playing: ", value=f"[{self.vc.track.title}]({self.vc.track.uri})")
        embed.set_footer(text=self.getProgressBar())
        await interaction.response.send_message(embed=embed)

    @discord.slash_command(name="queue", description="Displays the current queue")
    async def queue(self, interaction: discord.Interaction):
        embed = discord.Embed()
        if self.bot.user.avatar:
            embed.set_author(name=self.bot.user.name, icon_url=self.bot.user.avatar)
        else:
            embed.set_author(name=self.bot.user.name)
        print(self.vc.queue.count)
        if self.vc.queue.count > 10:
            songQueue = f''
            for x in range(10):
                songQueue += f"[{self.vc.queue[x].title}]({self.vc.queue[x].uri})\n"
            songQueue += f"..."
            embed.add_field(name=f'Queue', value=songQueue, inline=False)
        else:
            songQueue = f""
            for x in range(self.vc.queue.count):
                songQueue += f"[{self.vc.queue[x].title}]({self.vc.queue[x].uri})\n"
            songQueue += f"..."
            embed.add_field(name=f'Queue', value=songQueue, inline=False)
        await interaction.response.send_message(embed=embed)

    async def connect(self):
        await self.bot.wait_until_ready()
        await wavelink.NodePool.create_node(
            bot=self.bot,
            host="127.0.0.1",
            port=2333,
            password="supersecretpassword"
        )

    def getProgressBar(self) -> str:
        duration = self.vc.track.length
        position = int(self.vc.position)
        progress = int((position/duration) * 10)
        print(progress)
        bar = ""
        for x in range(10 - (10 - progress)):
            bar += "⬜"
        for x in range(10 - progress):
            bar += "⬛"
        bar +=  f"    {time.strftime('%H:%M:%S', time.gmtime(position))} / {time.strftime('%H:%M:%S', time.gmtime(duration))}"
        return bar

    @commands.Cog.listener()
    async def on_wavelink_node_ready(self, node: wavelink.Node):
        print(f'Node: {node.identifier} is ready')
        await self.bot.change_presence(activity=discord.Game(name="Praxis"), status=discord.Status.dnd)

    @commands.Cog.listener()
    async def on_wavelink_track_end(self, player: wavelink.Player, track, reason):
        if not player.queue.is_empty:
            next = player.queue.get()
            await player.play(next)

def setup(bot):
    bot.add_cog(Commands(bot))
