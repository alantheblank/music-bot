import discord
from discord.ext import commands
import json
import asyncio

bot = commands.Bot(command_prefix="!!", status=discord.Status.dnd, intents=discord.Intents.all())

if __name__ == "__main__":
    bot.load_extension("commands")
    bot.run(json.load(open("api.json"))["discord_api"])
